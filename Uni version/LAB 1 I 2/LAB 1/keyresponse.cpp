#include "camera.cpp"
#include <set>
class keyresponse
{

public:
    set<int> keys;
    int start = 6;
    int end = 48;
    float step = 6.0;
    float speed = 0.1;
    float turn = 0.01;
    keyresponse(){};
    keyresponse(float speed) { this->speed = speed; }
    keyresponse(float speed, float turn)
    {
        this->speed = speed;
        this->turn = turn;
    }

    void service(Camera &cam)
    {
        for (auto i = this->keys.begin(); i != this->keys.end(); i++)
        {
            int key = *i;

            switch ((int)key)
            {

            case 97:
                cam.turnH(-this->turn);
                break;

            case 100:
                cam.turnH(this->turn);
                break;

            case 119:
                cam.move(this->speed);
                break;
            case 115:
                cam.move(-this->speed);
                break;
            case 32:
                cam.vert(this->speed);
                break;
            case 99:
                cam.vert(-this->speed);
                break;
            case 113:
                cam.turnV(this->turn);
                break;
            case 101:
                cam.turnV(-this->turn);
                break;
            }
        }
    }

    void add(int key)
    {
        this->keys.insert(key);
    }
    void remove(int key)
    {
        this->keys.erase(key);
    }
};