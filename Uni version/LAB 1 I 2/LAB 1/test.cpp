//g++ -O3  -Iinclude -Llib -o test.exe test.cpp  -lfreeglut -lopengl32 -lglu32
#include "GL/glut.h" // GLUT, includes glu.h and gl.h
#include <math.h>
#include "camera.cpp"
#include "object.cpp"
#include "keyresponse.cpp"
#include "bspline.cpp"

//counters and stuff for the animation
int j;
int counter = 0, counterMax = 1000;
int z = 1;
//flags
int drawL = 1, drawS = 1, drawObj = 1;

#define FOV 90.0
#define FR 2000
#define NIR 0.1

//camera and keyboard objects
Camera cam(-3, 2, 35, 0, 0, 0);
keyresponse keys(0.005, 0.0005);
//drawn object and spile object
object obj;
bspline bs;

GLfloat lightpos[] = {0, 1, 0, 0};
GLfloat diff[] = {0.9, 0.9, 0.9, 0};
GLfloat rotatvec[] = {0, 1, 0};

void processNormalKeys(unsigned char key, int xx, int yy);
void upkey(unsigned char key, int xx, int yy);

void drawLight()
{
    // lightpos[2]+=0.005;
    // if (lightpos[2] > 20)
    //     lightpos[2] = -20;

    glDisable(GL_LIGHTING);
    // glBegin(GL_LINES);
    // glVertex3f(0,0,0);
    // glVertex3f(lightpos[0],lightpos[1],lightpos[2]);
    // glEnd();
    if (drawL)
    {

        glTranslatef(lightpos[0], lightpos[1], lightpos[2]);
        glutSolidCube(3);

        glTranslatef(-lightpos[0], -lightpos[1], -lightpos[2]);
    }
    if (drawS)
        bs.drawSpline();

    glEnable(GL_LIGHTING);
}
void changeSize(int w, int h)
{

    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if (h == 0)
        h = 1;
    float ratio = w * 1.0 / h;

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);

    // Reset Matrix
    glLoadIdentity();

    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);

    // Set the correct perspective.
    gluPerspective(FOV, ratio, NIR, FR);

    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}
void drawObject()
{
    drawLight();
    if (drawObj)
        obj.draw();
}

void renderScene(void)
{

    // Clear Color and Depth Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0, 0, 0, 1);
    glFrontFace(GL_CCW);

    // glFrontFace(GL_CCW);

    // Reset transformations

    glLoadIdentity();

    gluLookAt(cam.x, cam.y, cam.z,
              cam.x + cam.lx, cam.y + cam.ly, cam.z + cam.lz,
              0.0f, 1.0f, 0.0f);
    glShadeModel(GL_FLAT);

    glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diff);

    drawObject();

    glutSwapBuffers();
}
void upkey(unsigned char key, int xx, int yy)
{
    keys.remove(key);
}
void processNormalKeys(unsigned char key, int xx, int yy)
{

    // // cout << (int)key;
    if ((int)key == 27)
        exit(0);
    else
    {
        keys.add(key);
    }
}

void idle()
{

    counter++;
    if (counter > counterMax)
    {
        float angle = bs.calcAngle(rotatvec[0], rotatvec[1], rotatvec[2], bs.spline[j]);
        struct point p = bs.calcV(rotatvec[0], rotatvec[1], rotatvec[2], bs.tangent[j]);

        obj.vx = p.a[0];
        obj.vy = p.a[1];
        obj.vz = p.a[2];

        rotatvec[0] = bs.spline[j].a[0];
        rotatvec[1] = bs.spline[j].a[1];
        rotatvec[2] = bs.spline[j].a[2];
        cout << p.a[0] << " " << p.a[1] << " " << p.a[2]<<endl;
        obj.angle += angle;

        obj.move(bs.spline[j].a[0], bs.spline[j].a[1], bs.spline[j].a[2]);
        j += z;
        if (j == bs.spline.size() - 1)
            z = -1;
        if (j <= 0)
            z = 1;
        counter = 0;
    }
    keys.service(cam);
    //move on the spline
    //turn on the spline
    glutPostRedisplay();
}
vector<string> loadConfig()
{
    ifstream f;
    f.open("config.txt");
    string line;

    //LOAD THE FLAGS
    getline(f, line);
    drawObj = stoi(line);

    getline(f, line);
    drawL = stoi(line);

    getline(f, line);
    drawS = stoi(line);

    // LOAD CAMERA POSITION AND DIRECTION VECTOR
    getline(f, line);
    vector<string> listy = obj.split(line, ' ');
    Camera c(stof(listy[0]), stof(listy[1]), stof(listy[2]), stof(listy[3]), stof(listy[4]), stof(listy[5]));
    cam = c;
    cam.turnH(0); // reset camera angle to 0 at start, idk i forgot why this is like this

    // LOAD KEY SPEED -> CAMERA SPEED MOVEMENT
    getline(f, line);
    listy = obj.split(line, ' ');
    keyresponse k(stof(listy[0]), stof(listy[1]));
    keys = k;

    // LOAD LIGHT
    getline(f, line);
    listy = obj.split(line, ' ');
    lightpos[0] = stof(listy[0]);
    lightpos[1] = stof(listy[1]);
    lightpos[2] = stof(listy[2]);
    lightpos[3] = stof(listy[3]);

    // LOAD MAX COUNTER VALUE BEFORE DRAW IS CALLED ( BIGGER COUNTER SMALLER DRAW FREQUENCY )
    getline(f, line);
    counterMax = stoi(line);
    vector<string> output;

    // OBJECT PATH
    getline(f, line);
    output.push_back(line);

    // SPLINE PATH
    getline(f, line);
    output.push_back(line);
    f.close();

    return output;
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA | GLUT_MULTISAMPLE);

    glutInitWindowPosition(0, 0);
    glutInitWindowSize(1900, 1000);
    glutCreateWindow("");

    // OpenGL init
    // register callbacks
    glutDisplayFunc(renderScene);

    glutReshapeFunc(changeSize);
    glutIdleFunc(idle);
    glutKeyboardFunc(processNormalKeys);

    glutKeyboardUpFunc(upkey);
    // glutSpecialFunc(processSpecialKeys);

    // enter GLUT event processing cycle
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    // glCullFace(GL_BACK);
    // glEnable(GL_CULL_FACE);
    glEnable(GL_NORMALIZE);

    glEnable(GL_DEPTH_TEST);

    auto files = loadConfig();
    
    obj.loadFromFile(files[0]);
    bs.loadFromFile(files[1]);
    obj.angle = 90;
    cam.print();
    glutMainLoop();
    return 1;
}
