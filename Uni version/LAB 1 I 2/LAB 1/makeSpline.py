import numpy as np
import sys

controls = []
file = open(sys.argv[1])
tmax = 0
for line in file.readlines():
    splits = line.split(" ")
    if(len(splits) < 3):
        tmax = int(splits[0])
    else:
        controls.append([float(x) for x in splits])

B = [[-1, 3, -3, 1], [3, -6, 3, 0], [-3, 0, 3, 0], [1, 4, 1, 0]]
Bp = [[-1, 3, -3, 1], [2, -4, 2, 0], [-1, 0, 1, 0]]
B = np.array(B)
Bp = np.array(Bp)
P = []
n = len(controls)
print("--")
for i in range(1, n-3):
    for j in range(0, tmax+1):
        t = float(j)/float(tmax)
        T = np.array([t**3, t**2, t, 1])
        Tp = np.array([t**2, t, 1])
        R = np.array([controls[i-1], controls[i],
                      controls[i+1], controls[i+2]])

        p = list(1/6 * np.matmul(T, np.matmul(B, R)))

        pp = list(1/2 * np.matmul(Tp, np.matmul(Bp, R)))
        p.extend(pp)
        P.append(p)

print(P)
output = "default.txt"
if(len(sys.argv) > 2):
    output = sys.argv[2]
file = open(output, "w+")
ifo = 0
for x in P:
    file.write(""+str(x[0])+" "+str(x[1])+" "+str(x[2]) +
               " " + str(x[3]) + " " + str(x[4]) + " " + str(x[5]))
    ifo += 1
    if(ifo < len(P)-1):
        file.write("\n")
