//g++ -O3  -Iinclude -Llib -o test.exe test.cpp  -lfreeglut -lopengl32 -lglu32
#include "GL/glut.h" // GLUT, includes glu.h and gl.h
#include <math.h>
#include "camera.cpp"
#include "keyresponse.cpp"
#include <stdlib.h>
#include <time.h>
#include "scene.cpp"

//counters and stuff for the animation
float j = 0;
int counter = 0, counterMax = 1;

#define FOV 100.0
#define FR 1000
#define NIR 0.1
// object obj, obj1;

scene s;

//camera and keyboard objects
Camera cam(3, 0, 0, 1, 0, 0);
keyresponse keys(0.5, 0.1);

GLfloat lightpos[] = {0, 5, 0, 0};
GLfloat diff[] = {0.9, 0.9, 0.9, 0};

void processNormalKeys(unsigned char key, int xx, int yy);
void upkey(unsigned char key, int xx, int yy);

void changeSize(int w, int h)
{

    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if (h == 0)
        h = 1;
    float ratio = w * 1.0 / h;

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);

    // Reset Matrix
    glLoadIdentity();

    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);

    // Set the correct perspective.
    gluPerspective(FOV, ratio, NIR, FR);

    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}
void drawObject()
{
    s.draw(cam.x, cam.y, cam.z, cam.lx, cam.ly, cam.lz);
}

void renderScene(void)
{
    glClearColor(0, 0, 0, 1);
    // Clear Color and Depth Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    s.frontface();
    glLoadIdentity();

    gluLookAt(cam.x, cam.y, cam.z,
              cam.x + cam.lx, cam.y + cam.ly, cam.z + cam.lz,
              0.0f, 1.0f, 0.0f);

    glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diff);
    glShadeModel(GL_SMOOTH);

    drawObject();

    glutSwapBuffers();
}
void upkey(unsigned char key, int xx, int yy)
{
    keys.remove(key);
}
void processNormalKeys(unsigned char key, int xx, int yy)
{
    if ((int)key == 27)
        exit(0);
    else
    {
        keys.add(key);
    }
}

void idle()
{

    counter++;
    s.update(cam.x, cam.y, cam.z);

    keys.service(cam);
    glutPostRedisplay();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB | GLUT_MULTISAMPLE);

    printf("starting\n");
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(1900, 1000);
    glutCreateWindow("");

    s.loadFromFile((string) "../resources/scene.cfg");
    keys = keyresponse(s.keyws, s.keyad);
    glutDisplayFunc(renderScene);

    glutReshapeFunc(changeSize);
    glutIdleFunc(idle);
    glutKeyboardFunc(processNormalKeys);

    glutKeyboardUpFunc(upkey);
    // glutSpecialFunc(processSpecialKeys);

    // enter GLUT event processing cycle
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    s.cullface();
    glEnable(GL_CULL_FACE);
    glEnable(GL_NORMALIZE);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 1;
}
