# Scene Loader

A simple scene loader program using OpenGL, freeglut and the DevIL image loader library to display a scene defined in proprietary config files.

The program supports 3D object defined in .obj file format, it can display and drive a particle system using image types that the DevIL image loader supports and it can generate terrain based on a simplex noise function made by Sebastien Rombauts (https://github.com/SRombauts/SimplexNoise).

the proprietary format for the config file is described in the included example config file "scene.cfg"
the file has to be called scene.cfg and located in the resources folder 

i have included the compilation "script" and all the used libraries and dlls ( i think ), but im not sure how well it will compile on other machines, additionally the DevIL library has some issues when trying to get it up and running, so keep that in mind if you plan on compling the code.

The final program is compiled and located in the program folder
