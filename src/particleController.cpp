//controlls sprite particles
#include <vector>
#include <algorithm>
#include "sprite.cpp"
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <fstream>
using namespace std;

class particleControll
{

public:
    float g = 0.05;
    float x, y, z;
    bool init = false;

    float xRange[2] = {-0.5, 0.5};
    float yRange[2] = {1, 4};
    float zRange[2] = {-0.5, 0.5};
    float lifeRange[2] = {1, 5};
    float deathSpeedRange[2] = {0.05, 0.1};
    float scaleRange[2] = {0.1, 5.0};
    float speedRange[2] = {0.01, 0.1};
    GLfloat color[3] = {1.0, 1.0, 1.0};
    GLfloat mat[4] = {1.0, 1.0, 1.0, 1.0};
    bool Immortal = true;
    bool varyScale = true;
    bool varyDeath = true;
    int framerateCounter = 0, framerate = 2;
    struct particle
    {
        int id;
        float dist;
        float scale;
        float life;
        float lifeTotal;
        float deathSpeed;
        float speed;
        float pos[3];
        float dirVec[3];
        int type;
        bool dead;
    };
    bool useSprite = false;
    vector<particle> particleVec;
    sprites sprite;
    int number;
    GLfloat *origin;
    string file;
    particleControll() {}
    vector<string> split(string in, char del)
    {
        int i = 0;
        vector<string> p;
        string s = "";
        for (i; i < strlen(in.c_str()); i++)
        {
            if (in.c_str()[i] == del)
            {
                p.push_back(s);
                s = "";
            }
            else
                s += in.c_str()[i];
        }
        p.push_back(s);

        return p;
    }
    particleControll(string file, int numb, GLfloat *origin, bool useSprite)
    {
        this->x = origin[0];
        this->y = origin[1];
        this->z = origin[2];
        this->init = true;
        srand(time(NULL));
        this->useSprite = useSprite;
        if (useSprite)
            sprite = sprites(file, 1);
        this->number = numb;
        for (int i = 0; i < numb; i++)
            particleVec.push_back(generateParticle(i));
    }
    particleControll(string config, GLfloat *origin)
    {
        this->x = origin[0];
        this->y = origin[1];
        this->z = origin[2];
        this->init = true;
        srand(time(NULL));
        loadParticle(config);

        if (this->useSprite)
            sprite = sprites(this->file, 1);
        else
        {
            this->mat[0] = this->color[0];
            this->mat[1] = this->color[1];
            this->mat[2] = this->color[2];
        }
        for (int i = 0; i < this->number; i++)
            particleVec.push_back(generateParticle(i));
    }
    void loadParticle(string file)
    {
        ifstream f;
        f.open(file);
        string line;
        // get number
        getline(f, line);
        this->number = stoi(split(line, ':')[1]);
        // get g
        getline(f, line);
        this->g = stof(split(line, ':')[1]);
        //get xrange
        getline(f, line);
        this->xRange[0] = stof(split(split(line, ':')[1], ',')[0]);
        this->xRange[1] = stof(split(split(line, ':')[1], ',')[1]);

        //get yrange
        getline(f, line);
        this->yRange[0] = stof(split(split(line, ':')[1], ',')[0]);
        this->yRange[1] = stof(split(split(line, ':')[1], ',')[1]);

        //get zrange
        getline(f, line);
        this->zRange[0] = stof(split(split(line, ':')[1], ',')[0]);
        this->zRange[1] = stof(split(split(line, ':')[1], ',')[1]);

        //get liferange
        getline(f, line);
        this->lifeRange[0] = stof(split(split(line, ':')[1], ',')[0]);
        this->lifeRange[1] = stof(split(split(line, ':')[1], ',')[1]);

        //get deathspeedrange
        getline(f, line);
        this->deathSpeedRange[0] = stof(split(split(line, ':')[1], ',')[0]);
        this->deathSpeedRange[1] = stof(split(split(line, ':')[1], ',')[1]);

        //get scalerange
        getline(f, line);
        this->scaleRange[0] = stof(split(split(line, ':')[1], ',')[0]);
        this->scaleRange[1] = stof(split(split(line, ':')[1], ',')[1]);

        //get speedrange
        getline(f, line);
        this->speedRange[0] = stof(split(split(line, ':')[1], ',')[0]);
        this->speedRange[1] = stof(split(split(line, ':')[1], ',')[1]);

        //get varyscale
        getline(f, line);
        this->varyScale = stoi(split(line, ':')[1]);

        //get varydeath
        getline(f, line);
        this->varyDeath = stoi(split(line, ':')[1]);

        //get immortallity
        getline(f, line);
        this->Immortal = stoi(split(line, ':')[1]);

        //get usesprite
        getline(f, line);
        this->useSprite = stoi(split(line, ':')[1]);
        if (this->useSprite)
        //get filename
        {
            getline(f, line);
            this->file = split(line, ':')[1];
        }
        else
        {
            getline(f, line);
            this->color[0] = (GLfloat)stof(split(split(line, ':')[1], ',')[0]) / 255.0;
            this->color[1] = (GLfloat)stof(split(split(line, ':')[1], ',')[1]) / 255.0;
            this->color[2] = (GLfloat)stof(split(split(line, ':')[1], ',')[2]) / 255.0;
        }
    }

    particle generateParticle(int id)
    {
        struct particle out;
        out.id = id;
        out.lifeTotal = out.life = ((float)rand()) / RAND_MAX * (lifeRange[1] - lifeRange[0]) + lifeRange[0];

        if (varyDeath)
            out.deathSpeed = ((float)rand()) / RAND_MAX * (deathSpeedRange[1] - deathSpeedRange[0]) + deathSpeedRange[0];
        else
            out.deathSpeed = deathSpeedRange[0];

        if (varyScale)
            out.scale = ((float)rand()) / RAND_MAX * (scaleRange[1] - scaleRange[0]) + scaleRange[0];
        else
            out.scale = scaleRange[0];

        out.speed = ((float)rand()) / RAND_MAX * (speedRange[1] - speedRange[0]) + speedRange[0];

        out.pos[0] = this->x;
        out.pos[1] = this->y;
        out.pos[2] = this->z;

        out.dirVec[0] = ((float)rand()) / RAND_MAX * (xRange[1] - xRange[0]) + xRange[0];
        out.dirVec[1] = ((float)rand()) / RAND_MAX * (yRange[1] - yRange[0]) + yRange[0];
        out.dirVec[2] = ((float)rand()) / RAND_MAX * (zRange[1] - zRange[0]) + zRange[0];

        out.type = 0;
        out.dead = false;

        return out;
    }

    void draw(float camx, float camy, float camz)
    {

        if (!this->Immortal && this->particleVec.empty())
            return;
        if (!this->init)
            return;
        if (useSprite)
        {
            sprite.onOff(1);
            for (auto a = particleVec.begin(); a != particleVec.end(); a++)
                sprite.drawAt((*a).pos[0], (*a).pos[1], (*a).pos[2], (*a).scale, camx, camy, camz);
            sprite.onOff(0);

            return;
        }
        GLfloat mat2[] = {1, 1, 1, 1};
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, this->mat);
        for (auto a = particleVec.begin(); a != particleVec.end(); a++)
        {
            glPointSize((*a).scale * 10);
            glBegin(GL_POINTS);
            glVertex3fv((*a).pos);
            glEnd();
            glPointSize(1);
        }
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat2);
    }

    void update(float cx, float cy, float cz)
    {
        if (!this->init)
            return;
        framerateCounter++;
        if (framerateCounter < framerate)
            return;
        framerateCounter = 0;
        int deadCounter = 0;

        for (auto a = particleVec.begin(); a != particleVec.end(); a++)
        {
            (*a).pos[0] += (*a).speed * (*a).dirVec[0];
            (*a).pos[1] += (*a).speed * (*a).dirVec[1];
            (*a).pos[2] += (*a).speed * (*a).dirVec[2];

            (*a).dirVec[1] -= g;
            (*a).life -= (*a).deathSpeed;
            if ((*a).life <= 0)
            {
                (*a).dead = true;
                deadCounter++;
            }
        }
        vector<particle> newone;
        for (int i = 0; i < particleVec.size(); i++)
        {
            if (!particleVec[i].dead)
                newone.push_back(particleVec[i]);
        }
        if (!this->Immortal)
        {
            particleVec.clear();
            particleVec = newone;
            return;
        }

        deadCounter += (this->number + rand() % 3 - particleVec.size());

        for (int i = 0; i < deadCounter; i++)
            newone.push_back(generateParticle(2));
        particleVec.clear();
        particleVec = newone;
    }
};
//THIS ONE WORKS