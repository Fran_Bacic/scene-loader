// scene loader
#include <string>
#include <vector>
#include "particleController.cpp"
#include "object.cpp"
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <iostream>

using namespace std;
class scene
{
public:
    struct objectMods
    {
        float dx, dy, dz, rx, ry, rz, rangle;
        int count;
        float RX = 0, RY = 0, RZ = 0, RANGLE = 0;
        float VRX = 0, VRY = 1, VRZ = 0;
        object boi;
        bool animated = false;
    };
    bool front = false;
    float keyws = 0.5, keyad = 0.05;
    vector<sprites> sprite;
    vector<particleControll> particles;
    vector<objectMods> objects;
    vector<object> map;
    int globalCount = 0;
    float RENDER_DISTANCE = 200;
    float density = 0.05; //set the density to 0.3 which is acctually quite thick

    float fogColor[4] = {0.1, 0.2, 0.9, 0.8}; //set the for color to grey
    bool foggy = false;
    float heightStart = -1;
    float heightStop = 1;

    vector<string> split(string in, char del)
    {
        int i = 0;
        vector<string> p;
        string s = "";
        for (i; i < strlen(in.c_str()); i++)
        {
            if (in.c_str()[i] == del)
            {
                p.push_back(s);
                s = "";
            }
            else
            {
                s += in.c_str()[i];
            }
        }
        p.push_back(s);

        return p;
    }

    void frontface()
    {
        if (front)
            glFrontFace(GL_CW);
        else
            glFrontFace(GL_CCW);
    }
    void cullface()
    {
        if (!front)
            glCullFace(GL_BACK);
        else
            glCullFace(GL_FRONT);
    }
    void draw(float camx, float camy, float camz, float vx, float vy, float vz)
    {
        float dx = vx - camx;
        float dy = vz - camz;
        if (foggy)
        {
            if (camy >= this->heightStart && camy <= this->heightStop)
            {
                glEnable(GL_FOG);
            }
            else
            {
                glDisable(GL_FOG);
            }
        }

        // dx, dy are the normal to the tangent, to get the direction vector we swap their
        // positions and change the sign of one of them, thats why its +dx for ay
        // also in this case x and z are the plane coords but im denoting them as x and y

        float ax = camx - dy;
        float ay = camz + dx;

        float bx = camx + dy;
        float by = camz - dx;

        float nx = (vx - camx), ny = (vy - camy), nz = (vz - camz);

        float d = (nx * camx + ny * camy + nz * camz);

        for (auto a = map.begin(); a != map.end(); a++)
        {

            float px = ((*a).polys[0].a[0] + (*a).polys.back().c[0]) / 2;
            float py = ((*a).polys[0].a[2] + (*a).polys.back().c[2]) / 2;
            float D = (px - ax) * (by - ay) - (py - ay) * (bx - ax);
            if (D < 0 && sqrt(pow(camx - px * a->sx, 2) + pow(camz - py * a->sz, 2)) < RENDER_DISTANCE)
                (*a).draw();
        }
        for (auto a = objects.begin(); a != objects.end(); a++)
        {
            struct objectMods local = *a;

            for (int i = 0; i < a->count; i++)
            {

                glPushMatrix();
                glTranslatef(local.RX, local.RY, local.RZ);
                glRotatef(i * local.RANGLE, local.VRX, local.VRY, local.VRZ);
                glTranslatef(-local.RX, -local.RY, -local.RZ);

                local.boi.shift(i * local.dx, i * local.dy, i * local.dz);
                float shifty = 0.5 * sin((float)(this->globalCount) / 4000);

                float baseangle = local.boi.angle;
                float basex = local.boi.vx, basey = local.boi.vy, basez = local.boi.vz;
                local.boi.rotate(basex + local.rx, basey + local.ry, basez + local.rz, baseangle + i * local.rangle);

                // float D = nx * local.boi.x + ny * local.boi.y + nz * local.boi.z - d;
                if (local.animated)
                    local.boi.shift(0, shifty, 0);
                if (sqrt(pow(camx - local.boi.x, 2) + pow(camz - local.boi.z, 2)) < RENDER_DISTANCE)

                    local.boi.draw();
                if (local.animated)
                    local.boi.shift(0, -shifty, 0);
                local.boi.rotate(basex, basey, basez, baseangle);
                local.boi.shift(-i * local.dx, -i * local.dy, -i * local.dz);

                glPopMatrix();
                this->globalCount += rand() % 21 - 10;
            }
        }
        for (auto a = sprite.begin(); a != sprite.end(); a++)
            (*a).draw(camx, camy, camz);

        for (auto a = particles.begin(); a != particles.end(); a++)
            (*a).draw(camx, camy, camz);
    }

    void update(float camx, float camy, float camz)
    {
        for (auto a = particles.begin(); a != particles.end(); a++)
            (*a).update(camx, camy, camz);
    }

    float myRand()
    {
        return (rand() + rand() + rand()) / (3.0 * RAND_MAX);
    }
    void handleMap(string in)
    {
        vector<string> splits = split(in, ':');
        vector<string> base = split(splits[1], ',');

        float xmin = stof(base[0]);
        float xmax = stof(base[1]);
        float ymin = stof(base[2]);
        float ymax = stof(base[3]);
        int res = stoi(base[4]);
        float seed = stof(base[5]);
        int depth = stoi(base[6]);
        int powi = stoi(base[7]);
        float sizex = stof(base[8]);
        float sizey = stof(base[9]);
        float sizez = stof(base[10]);

        vector<string> count = split(splits[2], ',');
        int countx = stoi(count[0]);
        int county = stoi(count[1]);
        if (seed < 0)
        {
            seed = 4.12391239 * myRand();
        }
        for (int i = 0; i < county; i++)

            for (int j = 0; j < countx; j++)
            {
                object obj;
                obj.map = true;
                obj.genXY(xmin + j * (xmax - xmin), xmin + (j + 1) * (xmax - xmin),
                          ymin + i * (ymax - ymin), ymin + (i + 1) * (ymax - ymin),
                          res, seed, depth, powi);
                obj.scale(sizex, sizey, sizez);
                map.push_back(obj);
            }
    }
    void handleObject(string in)
    {
        vector<string> splits = split(in, ':');
        string file = splits[1];
        float basex = 0, basey = 0, basez = 0;
        float scalex = 1, scaley = 1, scalez = 1;
        float baserx = 0, basery = 1, baserz = 0, baseangle = 0;

        vector<string> base = split(splits[2], ',');
        basex = stof(base[0]);
        basey = stof(base[1]);
        basez = stof(base[2]);

        vector<string> scale = split(splits[3], ',');

        scalex = stof(scale[0]);
        scaley = stof(scale[1]);
        scalez = stof(scale[2]);

        vector<string> rotate = split(splits[4], ',');

        baserx = stof(rotate[0]);
        basery = stof(rotate[1]);
        baserz = stof(rotate[2]);
        baseangle = stof(rotate[3]);

        if (baseangle == 0.0)
            baseangle = myRand() * 360;
        struct objectMods local;

        local.count = stoi(splits[5]);
        local.animated = stoi(splits[6]);

        local.boi.scale(scalex, scaley, scalez);
        local.boi.move(basex, basey, basez);
        local.boi.rotate(baserx, basery, baserz, baseangle);

        local.boi.loadFromFile(file);

        if (local.count > 1)
        {
            local.boi.scale(scalex, scaley, scalez);

            vector<string> ds = split(splits[7], ',');
            local.dx = stof(ds[0]);
            local.dy = stof(ds[1]);
            local.dz = stof(ds[2]);

            vector<string> rs = split(splits[8], ',');
            local.rx = stof(rs[0]);
            local.ry = stof(rs[1]);
            local.rz = stof(rs[2]);
            local.rangle = stof(rs[3]);
            if (local.rangle == 0.0)
                local.rangle = myRand() * 360;

            vector<string> Rs = split(splits[9], ',');

            local.RX = stof(Rs[0]);
            local.RY = stof(Rs[1]);
            local.RZ = stof(Rs[2]);

            local.RANGLE = stof(Rs[3]);
            if (local.RANGLE == 0.0)
                local.RANGLE = myRand() * 360;

            vector<string> VRs = split(splits[10], ',');

            local.VRX = stof(VRs[0]);
            local.VRY = stof(VRs[1]);
            local.VRZ = stof(VRs[2]);
        }
        vector<string> rgba = split(splits[11], ',');
        local.boi.r = stof(rgba[0]);
        local.boi.g = stof(rgba[1]);
        local.boi.b = stof(rgba[2]);
        local.boi.a = stof(rgba[3]);
        this->objects.push_back(local);
    }
    void handleFog(string in)
    {
        this->foggy = true;
        vector<string> splits = split(in, ':');
        this->density = stof(splits[1]);
        vector<string> colors = split(splits[2], ',');

        this->fogColor[0] = stof(colors[0]);
        this->fogColor[1] = stof(colors[1]);
        this->fogColor[2] = stof(colors[2]);
        this->fogColor[3] = stof(colors[3]);

        this->heightStart = stof(splits[3]);
        this->heightStop = stof(splits[4]);

        glFogi(GL_FOG_MODE, GL_EXP2);          //set the fog mode to GL_EXP2
        glFogfv(GL_FOG_COLOR, this->fogColor); //set the fog color to our color chosen above

        glFogf(GL_FOG_DENSITY, this->density); //set the density to the value above

        glHint(GL_FOG_HINT, GL_NICEST);
    }
    void handleParticle(string in)
    {
        vector<string> splits = split(in, ':');
        string file = splits[1];
        float x = 0, y = 0, z = 0;
        if (splits.size() > 2)
        //    control = particleControll((string) "particleConfig.cfg", origin);
        {
            vector<string> pos = split(splits[2], ',');
            x = stof(pos[0]);
            y = stof(pos[1]);
            z = stof(pos[2]);
        }
        GLfloat origin[] = {x, y, z};
        particleControll a(file, origin);
        particles.push_back(a);
    }

    void loadFromFile(string file)
    {
        srand(time(NULL));
        for (int i = 0; i < rand() % 8; i++)
            rand();

        ifstream f;
        f.open(file);
        string line;

        while (getline(f, line))
        {

            vector<string> splits = split(line, ':');
            if (splits[0] == "RENDER_DISTANCE")
                this->RENDER_DISTANCE = stof(splits[1]);
            if (splits[0] == "KEYS")
            {
                keyws = stof(splits[1]);
                keyad = stof(splits[1]);
            }
            if (splits[0] == "MAP")
                handleMap(line);
            if (splits[0] == "OBJECT")
                handleObject(line);
            if (splits[0] == "PARTICLE")
                handleParticle(line);
            if (splits[0] == "FOG")
                handleFog(line);
            if (splits[0] == "CWCCW")
            {
                front = stoi(splits[1]);
            }
        }
        f.close();
    }

    // load file, file says all the things / links to other configs
    //CONFIG LAYOUT: OBJECT : CONFIG : COUNT : ORIGINS
};