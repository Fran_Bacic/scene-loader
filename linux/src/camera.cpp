#include <iostream>
#include <math.h>
#ifndef CAMERA_H
#define CAMERA_H
using namespace std;
class Camera
{
public:
    float x, y, z;
    float lx, ly, lz;
    float angleH = 0;
    float angleV = 0;
    bool moved = true;
    float point = 3;

    Camera() {}

    Camera(float x, float y, float z, float lx, float ly, float lz)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->lx = lx;
        this->ly = ly;
        this->lz = lz;
    }
    void move(float fraction)
    {
        x += lx * fraction;
        // y += ly * fraction;
        z += lz * fraction;
        this->moved = true;
        this->point = 3;
    }
    void turnH(float fraction)
    {
        this->angleH += fraction;
        lx = sin(this->angleH);
        lz = -cos(this->angleH);
        this->point = 3;
        this->moved = true;
    }
    void turnV(float fraction)
    {
        this->angleV += fraction;
        ly = sin(this->angleV);
        this->point = 3;
        this->moved = true;
    }
    void vert(float fractiom)
    {
        y += fractiom;
        this->point = 3;
        this->moved = true;
    }
    void set(float x, float y, float z, float lx, float ly, float lz)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->lx = lx;
        this->ly = ly;
        this->lz = lz;
        this->point = 3;
        this->moved = true;
    }
    void add(float f1, float f2, float f3)
    {
        this->x += f1;
        this->y += f2;
        this->z += f3;
        this->point = 3;
        this->moved = true;
    }
    void print()
    {
        cout << this->x << "\n";
        cout << this->y << "\n";
        cout << this->z << "\n";
        cout << this->lx << "\n";
        cout << this->ly << "\n";
        cout << this->lz << "\n";
    }
};



#endif