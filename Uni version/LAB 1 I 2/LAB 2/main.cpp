//g++ -O3  -Iinclude -Llib -o test.exe test.cpp  -lfreeglut -lopengl32 -lglu32
#include "GL/glut.h" // GLUT, includes glu.h and gl.h
#include <math.h>
#include "camera.cpp"
#include "keyresponse.cpp"
#include "particleController.cpp"
#include "object.cpp"
//counters and stuff for the animation
float j = 0;
int counter = 0, counterMax = 1;

#define FOV 90.0
#define FR 2000
#define NIR 0.1
object obj;

//camera and keyboard objects
Camera cam(16, 7, 0, -1, 0, 0);
keyresponse keys(0.05, 0.05);

GLfloat lightpos[] = {0, 1, 0, 0};
GLfloat diff[] = {0.9, 0.9, 0.9, 0};
GLfloat rotatvec[] = {0, 1, 0};

particleControll control, control2;
sprites tex;
void processNormalKeys(unsigned char key, int xx, int yy);
void upkey(unsigned char key, int xx, int yy);

void changeSize(int w, int h)
{

    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if (h == 0)
        h = 1;
    float ratio = w * 1.0 / h;

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);

    // Reset Matrix
    glLoadIdentity();

    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);

    // Set the correct perspective.
    gluPerspective(FOV, ratio, NIR, FR);

    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}
void drawObject()

{
    control.draw(cam.x, cam.y, cam.z);
    control2.draw(cam.x, cam.y, cam.z);

    tex.draw(cam.x, cam.y, cam.z);

    obj.draw();
}

void renderScene(void)
{

    glClearColor(0, 0, 0, 1);
    // Clear Color and Depth Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glFrontFace(GL_CCW);

    // glFrontFace(GL_CCW);

    // Reset transformations

    glLoadIdentity();

    gluLookAt(cam.x, cam.y, cam.z,
              cam.x + cam.lx, cam.y + cam.ly, cam.z + cam.lz,
              0.0f, 1.0f, 0.0f);
    glShadeModel(GL_FLAT);

    glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diff);

    drawObject();

    glutSwapBuffers();
}
void upkey(unsigned char key, int xx, int yy)
{
    keys.remove(key);
}
void processNormalKeys(unsigned char key, int xx, int yy)
{

    // // cout << (int)key;
    if ((int)key == 27)
        exit(0);
    else
    {
        keys.add(key);
    }
}

void idle()
{

    counter++;
    if (counter > counterMax)
    {
        control.update(cam.x, cam.y, cam.z);
        counter = 0;
    }
    keys.service(cam);
    glutPostRedisplay();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB | GLUT_MULTISAMPLE);

    glutInitWindowPosition(0, 0);
    glutInitWindowSize(1900, 1000);
    glutCreateWindow("");
    GLfloat origin[] = {4.6, 9.8, 0};
    GLfloat origin2[] = {4.6, 9.8, 0};

    obj.loadFromFile("vulcano.obj");
    control = particleControll((string) "particleConfig.cfg", origin);
    // control2 = particleControll((string) "particleConfig2.cfg", origin2);
    tex = sprites((string) "smoke.png",1);
    // OpenGL init
    // register callbacks
    glutDisplayFunc(renderScene);

    glutReshapeFunc(changeSize);
    glutIdleFunc(idle);
    glutKeyboardFunc(processNormalKeys);

    glutKeyboardUpFunc(upkey);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // glutSpecialFunc(processSpecialKeys);

    // enter GLUT event processing cycle
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    glEnable(GL_NORMALIZE);

    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 1;
}
