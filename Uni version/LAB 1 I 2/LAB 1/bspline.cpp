//b-spline object
#include <vector>
#include <fstream>
#include <cstring>
#include <string.h>
#include <string>
#include <iostream>
#include <math.h>
#include "GL/glut.h"
using namespace std;
struct point
{
    GLfloat a[3];
};

class bspline
{

public:
    int tmax = 100;
    float tanlength = 1;
    vector<point> control;
    vector<point> spline;
    vector<point> tangent;
    int n = 0;

    vector<string> split(string in, char del)
    {
        int i = 0;
        vector<string> p;
        string s = "";
        for (i; i < strlen(in.c_str()); i++)
        {
            if (in.c_str()[i] == del)
            {
                p.push_back(s);
                s = "";
            }
            else
            {
                s += in.c_str()[i];
            }
        }
        p.push_back(s);

        return p;
    }
    void loadFromFile(string file)
    {
        ifstream f;
        f.open(file);
        string line;
        getline(f, line);
        tmax = stoi(line);
        while (getline(f, line))
        {

            vector<string> listy = split(line, ' ');
            struct point p;

            p.a[0] = stof(listy[0]);
            p.a[1] = stof(listy[1]);
            p.a[2] = stof(listy[2]);
            spline.push_back(p);
            struct point t;
            t.a[0] = stof(listy[3]);
            t.a[1] = stof(listy[4]);
            t.a[2] = stof(listy[5]);
            tangent.push_back(t);
        }
        n = control.size() - 3;
        f.close();
    }


    float calcAngle(GLfloat x, GLfloat y, GLfloat z, point a)
    {
        float o = a.a[0] * x + a.a[1] * y + a.a[2] * z; // a*b
        float b = sqrt(x * x + y * y + z * z);
        float c = sqrt(a.a[0] * a.a[0] + a.a[1] * a.a[1] + a.a[2] * a.a[2]);
        float arc = o / (b * c);
        if (arc > 1)
            arc = 1;
        if (arc < -1)
            arc = -1;
        return acos(arc) * 180.0 / 3.14159;
    }
    point calcV(GLfloat a, GLfloat b, GLfloat c, point q)
    {
        point out;

        out.a[0] = b * q.a[2] - c * q.a[1];
        out.a[1] = -(a * q.a[2] - c * q.a[0]);
        out.a[2] = a * q.a[1] - b * q.a[0];
        return out;
    }
    void drawSpline()
    {
        glBegin(GL_LINE_STRIP);
        for (auto a = spline.begin(); a != spline.end(); a++)
        {
            glVertex3fv((*a).a);
        }

        glEnd();
        for (int i = 0; i < spline.size(); i++)
        {
            glBegin(GL_LINES);

            glVertex3fv(spline[i].a);

            point loc;
            loc.a[0] = spline[i].a[0] + tanlength * tangent[i].a[0];
            loc.a[1] = spline[i].a[1] + tanlength * tangent[i].a[1];
            loc.a[2] = spline[i].a[2] + tanlength * tangent[i].a[2];
            glVertex3fv(loc.a);
            glEnd();
        }
    }
};