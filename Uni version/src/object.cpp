//g++ -O3  -Iinclude -Llib -o object.exe object.cpp  -lfreeglut -lopengl32 -lglu32
#include <vector>
#include <fstream>
#include <cstring>
#include <string.h>
#include <string>
#include <iostream>
#include "GL/glut.h"
#include "SimplexNoise.cpp"

using namespace std;

struct polygon
{
    GLfloat a[3];
    GLfloat b[3];
    GLfloat c[3];
    GLfloat n[3];
};
struct p
{
    GLfloat a[3];
};
class object
{
public:
    bool map = false;
    float angle = 0;              //angle of rotation
    float vx = 0, vy = 1, vz = 0; // vector around which its rotated
    vector<polygon> polys;
    vector<p> normals;
    float x = 0.0, y = 0.0, z = 0.0;
    float sx = 1, sy = 1, sz = 1;
    float max = -100.0;
    float min = 1000;
    float r = 1, g = 1, b = 1, a = 1;

    vector<string> split(string in, char del)
    {
        int i = 0;
        vector<string> p;
        string s = "";
        for (i; i < strlen(in.c_str()); i++)
        {
            if (in.c_str()[i] == del)
            {
                p.push_back(s);
                s = "";
            }
            else
            {
                s += in.c_str()[i];
            }
        }
        p.push_back(s);

        return p;
    }

    p calcnorm(polygon a)
    {
        GLfloat one[3], two[3], cross[3];
        one[0] = a.b[0] - a.a[0];
        one[1] = a.b[1] - a.a[1];
        one[2] = a.b[2] - a.a[2];

        two[0] = a.c[0] - a.a[0];
        two[1] = a.c[1] - a.a[1];
        two[2] = a.c[2] - a.a[2];

        cross[0] = one[1] * two[2] - one[2] * two[1];
        cross[1] = -(one[0] * two[2] - one[2] * two[0]);
        cross[2] = one[0] * two[1] - one[1] * two[0];
        float c = sqrt(cross[0] * cross[0] + cross[1] * cross[1] + cross[2] * cross[2]);
        cross[0] /= c;
        cross[1] /= c;
        cross[2] /= c;

        p out;
        out.a[0] = cross[0];
        out.a[1] = cross[1];
        out.a[2] = cross[2];
        return out;
    }
    struct polygon makePoly(vector<string> in, vector<vector<float>> all)

    {
        vector<float> a = all[stoi(in[1]) - 1];
        vector<float> b = all[stoi(in[2]) - 1];
        vector<float> c = all[stoi(in[3]) - 1];
        GLfloat aa[3] = {(GLfloat)a[0], (GLfloat)a[1], (GLfloat)a[2]};
        GLfloat bb[3] = {(GLfloat)b[0], (GLfloat)b[1], (GLfloat)b[2]};
        GLfloat cc[3] = {(GLfloat)c[0], (GLfloat)c[1], (GLfloat)c[2]};

        struct polygon poly;
        poly.a[0] = aa[0];
        poly.a[1] = aa[1];
        poly.a[2] = aa[2];

        poly.b[0] = bb[0];
        poly.b[1] = bb[1];
        poly.b[2] = bb[2];

        poly.c[0] = cc[0];
        poly.c[1] = cc[1];
        poly.c[2] = cc[2];

        return poly;
    }

    void loadFromFile(string file)
    {
        ifstream f;
        f.open(file);
        if (f.is_open())
            cout << "i found the file" << endl;
        else
        {
            cout << " cant find file, exiting..." << endl;
            return;
        }

        string line;
        vector<vector<float>> all;
        long i = 0;
        vector<p> normals;
        while (getline(f, line))
        {
            vector<float> one;
            vector<string> listy = split(line, ' ');
            if (listy[0] == "v")
            {
                one.push_back(stof(listy[1]));
                one.push_back(stof(listy[2]));
                one.push_back(stof(listy[3]));
                all.push_back(one);
            }
            if (listy[0] == "vn")
            {
                p norm;
                norm.a[0] = stof(listy[1]);
                norm.a[1] = stof(listy[2]);
                norm.a[2] = stof(listy[3]);
                normals.push_back(norm);
            }

            if (listy[0] == "f")
            {

                // if its a file with already calculated normals, load those normals alongside the rest
                if (line.find("/") != string::npos)
                {
                    vector<string> inner1 = split(listy[1], '/');
                    vector<string> inner2 = split(listy[2], '/');
                    vector<string> inner3 = split(listy[3], '/');
                    // changing f 1/2/3 4/5/6 7/8/9 into
                    // f 1 4 7 and [3,6,9] ( 3 == 6 == 9 so imma just take 3)
                    vector<string> reconstructed = {"f", inner1[0], inner2[0], inner3[0]};
                    p norm = normals.at(stoi(inner1[2]) - 1);
                    polygon poly = makePoly(reconstructed, all);
                    poly.n[0] = norm.a[0];
                    poly.n[1] = norm.a[1];
                    poly.n[2] = norm.a[2];
                    polys.push_back(poly);
                }
                // if its a file without already calculated normals, calculate the normals
                else
                {
                    polygon poly = makePoly(listy, all);

                    p norm = calcnorm(poly);
                    poly.n[0] = norm.a[0];
                    poly.n[1] = norm.a[1];
                    poly.n[2] = norm.a[2];
                    polys.push_back(poly);
                }
            }
            // i++;
            // if (i % 1000 == 0)
            //     cout << i << endl;
        }
        f.close();
        normals.clear();
    }

    void print()
    {
        for (auto a = polys.begin(); a != polys.end(); a++)
        {
            cout << (*a).a[0] << " " << (*a).a[1] << " " << (*a).a[2] << endl;
            cout << (*a).b[0] << " " << (*a).b[1] << " " << (*a).b[2] << endl;
            cout << (*a).c[0] << " " << (*a).c[1] << " " << (*a).c[2] << endl;
            cout << (*a).n[0] << " " << (*a).n[1] << " " << (*a).n[2] << endl;
        }
    }
    void draw()
    {
        if (a < 1)
            glEnable(GL_BLEND);

        glTranslatef(x, y, z);

        glRotatef(angle, vx, vy, vz);
        glScalef(sx, sy, sz);
        GLfloat q[] = {r, g, b, a};
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, q);

        float green[] = {91 / 255.0, 138 / 255.0, 77 / 255.0};
        float gray[] = {173 / 255.0, 173 / 255.0, 173 / 255.0};
        float brown[] = {102 / 255.0, 61 / 255.0, 10 / 255.0};
        for (auto poly = polys.begin(); poly != polys.end(); poly++)
        {
            if (map)
            {
                float sim = poly->a[1] + poly->b[1] + poly->c[1];
                sim /= 3.0;
                float a = (tanh(sim + 1) + 1) / 2;
                float b = (tanh(sim - 1) + 1) / 2;

                float inter1[] = {(green[0] - brown[0]) * a + brown[0],
                                  (green[1] - brown[1]) * a + brown[1],
                                  (green[2] - brown[2]) * a + brown[2], 1};

                float inter2[] = {(gray[0] - green[0]) * b + green[0],
                                  (gray[1] - green[1]) * b + green[1],
                                  (gray[2] - green[2]) * b + green[2], 1};

                if (sim > -0.1)
                    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, inter2);
                else
                    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, inter1);
            }
            glBegin(GL_POLYGON);
            glNormal3fv(poly->n);
            glVertex3fv((*poly).a);
            glVertex3fv((*poly).b);
            glVertex3fv((*poly).c);
            glEnd();
        }
        glScalef(1 / sx, 1 / sy, 1 / sz);

        glRotatef(-angle, vx, vy, vz);

        glTranslatef(-x, -y, -z);
        if (a < 1)
            glDisable(GL_BLEND);
    }
    void move(float ax, float ay, float az)

    {
        x = ax;
        y = ay;
        z = az;
    }
    void shift(float xm, float ym, float zm)
    {
        x += xm;
        y += ym;
        z += zm;
    }
    void rotate(float vvx, float vvy, float vvz, float vangle)
    {
        vx = vvx;
        vy = vvy;
        vz = vvz;
        angle = vangle;
    }
    double ff(int i, float maxi, int j, float maxj, int f, float seed, int powi)
    {
        double s = 0;

        for (int k = -3; k < f; k++)
        {

            double p = pow(2, k);
            double n = pow(2, -k);
            double e = pow(2, -k);
            if (k < 1)
                e = 1;
            s += e * SimplexNoise::noise(p * (float)i / maxi + seed * p, p * (float)j / maxj + seed * p);
        }

        // return succ(s * H + offset, i, j, maxi, maxj, maxj);
        return pow(s, powi);
    }
    void generateTerrain(float width, int height, int count, int depth, float seed, float H, int powi)
    {

        float unit = (float)1.0 / (float)count;
        sx = width;
        sy = H;
        sz = height;

        for (int i = 0; i < count - 1; i++)
            for (int j = 0; j < count - 1; j++)
            {
                polygon p1;
                p1.a[0] = (float)j * unit;
                p1.a[1] = ff(i, count, j, count, depth, seed, powi);
                p1.a[2] = (float)i * unit;

                p1.b[0] = (float)j * unit;
                p1.b[1] = ff(i + 1, count, j, count, depth, seed, powi);
                p1.b[2] = (float)(i + 1) * unit;

                p1.c[0] = (float)(j + 1) * unit;
                p1.c[1] = ff(i, count, j + 1, count, depth, seed, powi);
                p1.c[2] = (float)i * unit;

                p n = calcnorm(p1);

                p1.n[0] = n.a[0];
                p1.n[1] = n.a[1];
                p1.n[2] = n.a[2];

                polygon p2;
                p2.a[0] = (float)j * unit;
                p2.a[1] = ff(i + 1, count, j, count, depth, seed, powi);
                p2.a[2] = (float)(i + 1) * unit;

                p2.b[0] = (float)(j + 1) * unit;
                p2.b[1] = ff(i + 1, count, j + 1, count, depth, seed, powi);
                p2.b[2] = (float)(i + 1) * unit;

                p2.c[0] = (float)(j + 1) * unit;
                p2.c[1] = ff(i, count, j + 1, count, depth, seed, powi);
                p2.c[2] = (float)i * unit;

                n = calcnorm(p2);

                p2.n[0] = n.a[0];
                p2.n[1] = n.a[1];
                p2.n[2] = n.a[2];

                polys.push_back(p1);
                polys.push_back(p2);
            }
    }
    double ff2(float x, float y, int f, float seed, int powi)
    {
        double s = 0;

        for (int k = -3; k < f; k++)
        {

            double p = pow(2, k);
            double n = pow(2, -k);
            double e = pow(2, -k);
            if (k < 1)
                e = 1;
            s += e * SimplexNoise::noise(p * (x + seed), p * (y + seed));
        }

        // return succ(s * H + offset, i, j, maxi, maxj, maxj);
        return pow(s, powi);
    }

    void genXY(float xmin, float xmax, float ymin, float ymax, int count, float seed, int depth, int powi)
    {

        float width = xmax - xmin;
        float height = ymax - ymin;

        for (int i = 0; i < count; i++)
            for (int j = 0; j < count; j++)
            {
                polygon p1;
                float x0 = (float)j * width / (float)count + xmin;
                float x1 = (float)(j + 1) * width / (float)count + xmin;
                float y0 = (float)i * height / (float)count + ymin;
                float y1 = (float)(i + 1) * height / (float)count + ymin;

                p1.a[0] = x0;
                p1.a[1] = ff2(x0, y0, depth, seed, powi);
                p1.a[2] = y0;

                p1.b[0] = x0;
                p1.b[1] = ff2(x0, y1, depth, seed, powi);
                p1.b[2] = y1;

                p1.c[0] = x1;
                p1.c[1] = ff2(x1, y0, depth, seed, powi);
                p1.c[2] = y0;

                p n = calcnorm(p1);

                p1.n[0] = n.a[0];
                p1.n[1] = n.a[1];
                p1.n[2] = n.a[2];

                polygon p2;
                p2.a[0] = x0;
                p2.a[1] = ff2(x0, y1, depth, seed, powi);
                p2.a[2] = y1;

                p2.b[0] = x1,
                p2.b[1] = ff2(x1, y1, depth, seed, powi);
                p2.b[2] = y1;

                p2.c[0] = x1;
                p2.c[1] = ff2(x1, y0, depth, seed, powi);
                p2.c[2] = y0;

                n = calcnorm(p2);

                p2.n[0] = n.a[0];
                p2.n[1] = n.a[1];
                p2.n[2] = n.a[2];

                polys.push_back(p1);
                polys.push_back(p2);

                if (p1.a[1] > this->max)
                    this->max = p1.a[1];
                if (p1.a[1] < this->min)
                    this->min = p1.a[1];

                if (p1.b[1] > this->max)
                    this->max = p1.b[1];
                if (p1.b[1] < this->min)
                    this->min = p1.b[1];

                if (p1.c[1] > this->max)
                    this->max = p1.c[1];
                if (p1.c[1] < this->min)
                    this->min = p1.c[1];

                if (p2.a[1] > this->max)
                    this->max = p2.a[1];
                if (p2.a[1] < this->min)
                    this->min = p2.a[1];

                if (p2.b[1] > this->max)
                    this->max = p2.b[1];
                if (p2.b[1] < this->min)
                    this->min = p2.b[1];

                if (p2.c[1] > this->max)
                    this->max = p2.c[1];
                if (p2.c[1] < this->min)
                    this->min = p2.c[1];
            }
    }
    void scale(float v)
    {
        sx = v;
        sy = v;
        sz = v;
    }
    void scale(float xs, float ys, float zs)
    {
        sx = xs;
        sy = ys;
        sz = zs;
    }
};
