//g++ -O3  -Iinclude -Llib -o object.exe object.cpp  -lfreeglut -lopengl32 -lglu32
#include <vector>
#include <fstream>
#include <cstring>
#include <string.h>
#include <string>
#include <iostream>
#include "GL/glut.h"

using namespace std;

struct polygon
{
    GLfloat a[3];
    GLfloat b[3];
    GLfloat c[3];
    GLfloat n[3];
};
struct p
{
    GLfloat a[3];
};
class object
{
public:
    GLfloat angle = 0;              //angle of rotation
    GLfloat vx = 0, vy = 1, vz = 0; // vector around which its rotated
    vector<polygon> polys;
    vector<p> normals;
    GLfloat x = 04.0, y = 0.0, z = 0.0;

    vector<string> split(string in, char del)
    {
        int i = 0;
        vector<string> p;
        string s = "";
        for (i; i < strlen(in.c_str()); i++)
        {
            if (in.c_str()[i] == del)
            {
                p.push_back(s);
                s = "";
            }
            else
            {
                s += in.c_str()[i];
            }
        }
        p.push_back(s);

        return p;
    }

    p calcnorm(polygon a)
    {
        GLfloat one[3], two[3], cross[3];
        one[0] = a.b[0] - a.a[0];
        one[1] = a.b[1] - a.a[1];
        one[2] = a.b[2] - a.a[2];

        two[0] = a.c[0] - a.a[0];
        two[1] = a.c[1] - a.a[1];
        two[2] = a.c[2] - a.a[2];

        cross[0] = one[1] * two[2] - one[2] * two[1];
        cross[1] = -(one[0] * two[2] - one[2] * two[0]);
        cross[2] = one[0] * two[1] - one[1] * two[0];
        float c = sqrt(cross[0] * cross[0] + cross[1] * cross[1] + cross[2] * cross[2]);
        cross[0] /= c;
        cross[1] /= c;
        cross[2] /= c;

        p out;
        out.a[0] = cross[0];
        out.a[1] = cross[1];
        out.a[2] = cross[2];
        return out;
    }
    struct polygon makePoly(vector<string> in, vector<vector<float>> all)

    {
        vector<float> a = all[stoi(in[1]) - 1];
        vector<float> b = all[stoi(in[2]) - 1];
        vector<float> c = all[stoi(in[3]) - 1];
        GLfloat aa[3] = {(GLfloat)a[0], (GLfloat)a[1], (GLfloat)a[2]};
        GLfloat bb[3] = {(GLfloat)b[0], (GLfloat)b[1], (GLfloat)b[2]};
        GLfloat cc[3] = {(GLfloat)c[0], (GLfloat)c[1], (GLfloat)c[2]};

        struct polygon poly;
        poly.a[0] = aa[0];
        poly.a[1] = aa[1];
        poly.a[2] = aa[2];

        poly.b[0] = bb[0];
        poly.b[1] = bb[1];
        poly.b[2] = bb[2];

        poly.c[0] = cc[0];
        poly.c[1] = cc[1];
        poly.c[2] = cc[2];

        return poly;
    }

    void loadFromFile(string file)
    {
        ifstream f;
        f.open(file);
        string line;
        vector<vector<float>> all;
        long i = 0;
        vector<p> normals;
        while (getline(f, line))
        {
            vector<float> one;
            vector<string> listy = split(line, ' ');
            if (listy[0] == "v")
            {
                one.push_back(stof(listy[1]));
                one.push_back(stof(listy[2]));
                one.push_back(stof(listy[3]));
                all.push_back(one);
            }
            if (listy[0] == "vn")
            {
                p norm;
                norm.a[0] = stof(listy[1]);
                norm.a[1] = stof(listy[2]);
                norm.a[2] = stof(listy[3]);
                normals.push_back(norm);
            }

            if (listy[0] == "f")
            {

                // if its a file with already calculated normals, load those normals alongside the rest
                if (line.find("/") != string::npos)
                {
                    vector<string> inner1 = split(listy[1], '/');
                    vector<string> inner2 = split(listy[2], '/');
                    vector<string> inner3 = split(listy[3], '/');
                    // changing f 1/2/3 4/5/6 7/8/9 into
                    // f 1 4 7 and [3,6,9] ( 3 == 6 == 9 so imma just take 3)
                    vector<string> reconstructed = {"f", inner1[0], inner2[0], inner3[0]};
                    p norm = normals.at(stoi(inner1[2]) - 1);
                    polygon poly = makePoly(reconstructed, all);
                    poly.n[0] = norm.a[0];
                    poly.n[1] = norm.a[1];
                    poly.n[2] = norm.a[2];
                    polys.push_back(poly);
                }
                // if its a file without already calculated normals, calculate the normals
                else
                {
                    polygon poly = makePoly(listy, all);

                    p norm = calcnorm(poly);
                    poly.n[0] = norm.a[0];
                    poly.n[1] = norm.a[1];
                    poly.n[2] = norm.a[2];
                    polys.push_back(poly);
                }
            }
            i++;
            if (i % 1000 == 0)
                cout << i << endl;
        }
        f.close();
        normals.clear();
    }

    void print()
    {
        for (auto a = polys.begin(); a != polys.end(); a++)
        {
            cout << (*a).a[0] << " " << (*a).a[1] << " " << (*a).a[2] << endl;
            cout << (*a).b[0] << " " << (*a).b[1] << " " << (*a).b[2] << endl;
            cout << (*a).c[0] << " " << (*a).c[1] << " " << (*a).c[2] << endl;
            cout << (*a).n[0] << " " << (*a).n[1] << " " << (*a).n[2] << endl;
        }
    }
    void draw()
    {

        glTranslatef(x, y, z);
        glRotatef(angle, vx, vy, vz);
        GLfloat q[] = {1, 1, 1, 1};
        for (auto poly = polys.begin(); poly != polys.end(); poly++)
        {
            glColor3f(1, 1, 1);
            glBegin(GL_POLYGON);
            glNormal3fv(poly->n);
            glVertex3fv((*poly).a);
            glVertex3fv((*poly).b);
            glVertex3fv((*poly).c);
            glEnd();
        }
        glRotatef(-angle, vx, vy, vz);

        glTranslatef(-x, -y, -z);
    }
    void move(GLfloat ax, GLfloat ay, GLfloat az)
    {
        x = ax;
        y = ay;
        z = az;
    }
};
